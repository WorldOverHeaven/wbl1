package tasks

import (
	"errors"
	"fmt"
)

func Task8() {
	var a int64
	a = 7
	fmt.Printf("%08b\n", a)
	SetBit(1, 0, &a)
	fmt.Printf("%08b\n", a)
	SetBit(7, 1, &a)
	fmt.Printf("%08b\n", a)
}

func setBit(pos int, val int, source *int64) {
	var a int64
	a = 1 << pos

	if val == 0 {
		*source = *source ^ a
	}
	if val == 1 {
		*source = *source | a
	}
}

func SetBit(pos int, val int, source *int64) error {
	if val != 1 && val != 0 {
		return errors.New("incorrect val value")
	}

	if pos < 0 || pos >= 64 {
		return errors.New("incorrect pos value")
	}
	setBit(pos, val, source)
	return nil
}
