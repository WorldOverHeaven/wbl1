package tasks

import (
	"fmt"
	"sync"
)

func Task9() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	arr := make([]int, 10, 10)
	for i := 0; i < len(arr); i++ {
		arr[i] = i + 1
	}

	go func() {
		for _, i := range arr {
			ch1 <- i
		}
		close(ch1)
	}()

	go func() {
		for i := range ch1 {
			ch2 <- i * i
		}
		close(ch2)
	}()

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func() {
		for i := range ch2 {
			fmt.Println(i)
		}
		wg.Done()
	}()

	wg.Wait()
}
