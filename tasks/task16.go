package tasks

import (
	"errors"
	"fmt"
	"math/rand"
)

func Task16() {
	arr := make([]int, 10)

	for i := 0; i < len(arr); i++ {
		arr[i] = rand.Intn(5) + 1
	}

	fmt.Println(arr)

	quickSort(arr, 0, len(arr)-1)

	for i := 0; i < len(arr)-1; i++ {
		if arr[i] > arr[i+1] {
			err := errors.New("bad sort")
			panic(err)
		}
	}

	fmt.Println(arr)
}

func quickSort(arr []int, left int, right int) {
	if left >= right {
		return
	}
	pivotIndex := partition(arr, left, right)
	quickSort(arr, left, pivotIndex-1)
	quickSort(arr, pivotIndex+1, right)
}

func partition(arr []int, left int, right int) int {
	pivotIndex := rand.Intn(right-left) + left
	arr[right], arr[pivotIndex] = arr[pivotIndex], arr[right]

	pivot := arr[right]
	i := left - 1

	for j := left; j < right; j++ {
		if arr[j] <= pivot {
			i++
			arr[i], arr[j] = arr[j], arr[i]
		}
	}

	arr[i+1], arr[right] = arr[right], arr[i+1]
	return i + 1
}
