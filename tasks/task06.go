package tasks

import (
	"context"
	"fmt"
	"sync"
	"time"
)

// context
// chan

func Task6() {
	chanAbort()
	chanClose()
	contextClose()
}

func chanAbort() {
	wg := &sync.WaitGroup{}
	wg.Add(1)
	x := 0

	abort := make(chan struct{})
	go func() {
		for {
			select {
			case <-abort:
				wg.Done()
				return
			default:
				fmt.Println(x)
				x += 1
				time.Sleep(time.Millisecond * 100)
			}
		}
	}()

	time.Sleep(time.Second * 2)
	abort <- struct{}{}

	wg.Wait()
	fmt.Println("finish")
}

func chanClose() {
	wg := &sync.WaitGroup{}
	wg.Add(1)

	x := 0

	ch := make(chan struct{})
	go func() {
		for {
			select {
			case _, ok := <-ch:
				if !ok {
					wg.Done()
					return
				} else {
					fmt.Println(x)
					x += 1
					time.Sleep(time.Millisecond * 500)
				}
			}
		}
	}()

	ch <- struct{}{}
	ch <- struct{}{}
	ch <- struct{}{}
	close(ch)

	wg.Wait()
	fmt.Println("finish")
}

func contextClose() {
	ch := make(chan struct{})
	ctx, cancel := context.WithCancel(context.Background())

	x := 0

	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				ch <- struct{}{}
				return
			default:
				fmt.Println(x)
				x += 1
				time.Sleep(200 * time.Millisecond)
			}
		}
	}(ctx)

	go func() {
		time.Sleep(3 * time.Second)
		cancel()
	}()

	<-ch
	fmt.Println("finish")
}
