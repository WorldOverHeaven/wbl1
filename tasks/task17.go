package tasks

import (
	"errors"
	"fmt"
)

func Task17() {
	arr := make([]int, 10, 10)
	for i := 0; i < 10; i++ {
		arr[i] = i * i
	}
	fmt.Println(arr)
	fmt.Println(binarySearch(arr, 4))
	fmt.Println(binarySearch(arr, 5))
}

func binarySearch(nums []int, target int) (int, error) {
	if target > nums[len(nums)-1] || target < nums[0] {
		return -1, errors.New("target not in array")
	}

	left := 0
	right := len(nums)
	mid := (left + right) / 2
	for nums[mid] != target && left <= right {
		if nums[mid] == target {
			return mid, nil
		}
		if nums[mid] > target {
			right = mid - 1
			mid = (left + right) / 2
		}
		if nums[mid] < target {
			left = mid + 1
			mid = (left + right) / 2
		}
	}
	if nums[mid] == target {
		return mid, nil
	}
	return -1, errors.New("target not in array")
}
