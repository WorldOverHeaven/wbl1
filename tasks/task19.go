package tasks

import (
	"bytes"
	"fmt"
)

func Task19() {
	s := "Тестовая строка"
	fmt.Println(s)
	fmt.Println(Reverse(s))
	fmt.Println(Reverse2(s))
	fmt.Println(Reverse3(s))
}

func Reverse(s string) string {
	var buffer bytes.Buffer

	rs := []rune(s)

	for i := len(rs) - 1; i >= 0; i-- {
		buffer.WriteRune(rs[i])
	}

	return buffer.String()
}

func Reverse2(s string) string {
	var buffer bytes.Buffer

	for i := len([]rune(s)) - 1; i >= 0; i-- {
		buffer.WriteRune([]rune(s)[i])
	}

	return buffer.String()
}

func Reverse3(s string) string {
	res := make([]rune, len([]rune(s)))

	for i := 0; i < len(res); i++ {
		res[i] = ([]rune(s))[len(res)-i-1]
	}

	return string(res)
}
