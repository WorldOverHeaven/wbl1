package tasks

import (
	"errors"
	"fmt"
)

func Task23() {
	arr := make([]int, 6, 6)
	for i := range arr {
		arr[i] = i
	}
	fmt.Println(arr)
	arr, _ = Delete2(arr, 4)
	fmt.Println(arr)
	arr, _ = Delete2(arr, 0)
	fmt.Println(arr)
}

func Delete(arr []int, index int) ([]int, error) {
	if index < 0 || index >= len(arr) {
		return nil, errors.New("incorrect index")
	}
	for i := index + 1; i < len(arr); i++ {
		arr[i-1] = arr[i]
	}
	return arr[:len(arr)-1], nil
}

func Delete2(arr []int, index int) ([]int, error) {
	if index < 0 || index >= len(arr) {
		return nil, errors.New("incorrect index")
	}
	arr = append(arr[:index], arr[index+1:]...)
	return arr, nil
}
