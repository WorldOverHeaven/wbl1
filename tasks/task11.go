package tasks

import "fmt"

func Task11() {
	set1 := make(map[string]struct{})
	set2 := make(map[string]struct{})

	set1["a"] = struct{}{}
	set1["b"] = struct{}{}

	set2["b"] = struct{}{}
	set2["c"] = struct{}{}

	set := Intersection(set1, set2)
	fmt.Println(set)
}

func Intersection(set1, set2 map[string]struct{}) map[string]struct{} {
	set := make(map[string]struct{})
	if len(set2) < len(set1) {
		set2, set1 = set1, set2
		// Сделано для оптимизации
	}

	for val, _ := range set1 {
		if _, ok := set2[val]; ok {
			set[val] = struct{}{}
		}
	}
	return set
}
