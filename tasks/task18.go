package tasks

import (
	"sync"
	"sync/atomic"
)

type Counter1 struct {
	counter atomic.Int64
}

func NewCounter1() Counter1 {
	return Counter1{
		counter: atomic.Int64{},
	}
}

func (c *Counter1) Add() {
	c.counter.Add(1)
}

//////////////

type Counter2 struct {
	counter int64
	mu      sync.Mutex
}

func NewCounter2() Counter2 {
	return Counter2{
		counter: 0,
		mu:      sync.Mutex{},
	}
}

func (c *Counter2) Add() {
	c.mu.Lock()
	c.counter++
	c.mu.Unlock()
}
