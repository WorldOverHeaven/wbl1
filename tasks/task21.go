package tasks

import "fmt"

func Task21() {
	doc := &JsonDocument{}
	adapter := &JsonDocumentAdapter{jsonDocument: doc}
	adapter.SendXmlData()
}

// Адаптер - это структурный паттерн проектирования, который позволяет объектам с несовместимыми интерфейсами работать вместе.

// Есть сервис, который работает с данными в формате XML

type DataService interface {
	SendXmlData()
}

type XmlDocument struct {
}

func (doc *XmlDocument) SendXmlData() {
	fmt.Println("Отправка xml")
}

/// Есть сервис, который работает с данными в формате JSON

type JsonDocument struct {
}

func (doc *JsonDocument) ConvertToXml() XmlDocument {
	return XmlDocument{}
}

/// Для возможности использовать один сервис из другого создается адаптер

type JsonDocumentAdapter struct {
	jsonDocument *JsonDocument
}

func (adapter JsonDocumentAdapter) SendXmlData() {
	xml := adapter.jsonDocument.ConvertToXml()
	xml.SendXmlData()
}
