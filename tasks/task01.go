package tasks

import "fmt"

type Human struct {
}

type Action struct {
	Human
}

func (*Human) Say() {
	fmt.Println("Hello")
}

func (*Action) Say() {
	fmt.Println("World")
}

func Task1() {
	a := Action{Human{}}
	a.Say()
}

// В go нет наследования, но есть встраивание
//
