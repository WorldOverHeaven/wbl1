package tasks

import "fmt"

func Task12() {
	multiSet := make(map[string]int)
	multiSet["cat"] += 1
	multiSet["cat"] += 1
	multiSet["dog"] += 1
	multiSet["cat"] += 1
	multiSet["tree"] += 1

	fmt.Println("Multiset")
	fmt.Println(multiSet)

	set := make(map[string]struct{})
	set["cat"] = struct{}{}
	set["cat"] = struct{}{}
	set["dog"] = struct{}{}
	set["cat"] = struct{}{}
	set["tree"] = struct{}{}

	fmt.Println("Set")
	fmt.Println(set)
}
