package tasks

import (
	"bytes"
	"fmt"
)

var justString string

func createHugeString(len int) string {
	var buffer bytes.Buffer

	for i := 0; i < len; i++ {
		buffer.WriteString("a")
	}
	return buffer.String()
}

func someFunc() {
	v := createHugeString(1 << 10)
	justString = v[:100]
}

func Task15() {
	someFunc()

	// Нормальный вариант.
	var justString2 string
	justString2 = someFunc2()

	fmt.Println(justString2)
	// Теперь нет глобальных переменных.
	// А также память не будет занята лишними данными.
}

func someFunc2() string {
	v := createHugeString(1 << 10)

	var buffer bytes.Buffer

	for _, val := range v[:100] {
		buffer.WriteRune(val)
	}
	return buffer.String()
}

// Замечания
// 1 Глобальная переменная justString.
// Глобальные переменные лучше не использовать.
// 2 Слайс не создает новый массив, а ссылается на базовый массив.
// Поэтому в памяти будет храниться 2^10 - 100 бесполезных данных, т. к. они не используются.
//
