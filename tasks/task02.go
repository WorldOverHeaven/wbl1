package tasks

import (
	"fmt"
	"sync"
)

func calculate(arr []int) {
	wg := &sync.WaitGroup{}
	wg.Add(len(arr))
	for i := 0; i < len(arr); i++ {
		i := i
		go func(i int) {
			fmt.Println(arr[i] * arr[i])
			wg.Done()
		}(i)
	}
	wg.Wait()
}

func calculate2(arr []int) {
	res := make([]int, len(arr))

	wg := &sync.WaitGroup{}
	wg.Add(len(arr))
	for i := 0; i < len(arr); i++ {
		i := i
		go func(i int) {
			res[i] = arr[i] * arr[i]
			wg.Done()
		}(i)
	}
	wg.Wait()

	for _, val := range res {
		fmt.Println(val)
	}
}

func Task2() {
	arr := make([]int, 5)
	arr[0] = 2
	arr[1] = 4
	arr[2] = 6
	arr[3] = 8
	arr[4] = 10
	calculate(arr)
	calculate2(arr)
}
