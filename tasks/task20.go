package tasks

import (
	"bytes"
	"fmt"
)

func Task20() {
	s := "Привет мир !!! 123 234 345"
	fmt.Println(reverseWords(s))
}

func reverseWords(s string) string {
	words := make([]string, 0)
	buffer := bytes.Buffer{}
	for _, val := range s {
		if val == ' ' {
			words = append(words, buffer.String())
			buffer.Reset()
		} else {
			buffer.WriteRune(val)
		}
	}
	words = append(words, buffer.String())
	buffer.Reset()

	for i := len(words) - 1; i > 0; i-- {
		buffer.WriteString(words[i])
		buffer.WriteRune(' ')
	}
	buffer.WriteString(words[0])

	return buffer.String()
}
