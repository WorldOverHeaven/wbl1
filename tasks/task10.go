package tasks

import (
	"fmt"
	"math"
)

func Task10() {
	arr := make([]float64, 8)
	arr[0] = -25.4
	arr[1] = -27.0
	arr[2] = 13.0
	arr[3] = 19.0
	arr[4] = 15.5
	arr[5] = 24.5
	arr[6] = -21.0
	arr[7] = 32.5
	m := createTemperatureMap(arr)
	fmt.Println(m)
}

func createTemperatureMap(arr []float64) map[int][]float64 {
	m := make(map[int][]float64)
	for _, val := range arr {
		tmp := int(math.Round(val)) / 10 * 10
		if _, ok := m[tmp]; ok {
			m[tmp] = append(m[tmp], val)
		} else {
			m[tmp] = []float64{val}
		}
	}
	return m
}
