package tasks

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func Task3() {
	arr := make([]int, 5)
	arr[0] = 2
	arr[1] = 4
	arr[2] = 6
	arr[3] = 8
	arr[4] = 10
	task3(arr)
}

func task3(arr []int) {
	var sum atomic.Int64
	wg := &sync.WaitGroup{}
	wg.Add(len(arr))
	for i := 0; i < len(arr); i++ {
		i := i
		go func(i int) {
			sum.Add(int64(arr[i]) * int64(arr[i]))
			wg.Done()
		}(i)
	}
	wg.Wait()
	fmt.Println(sum.Load())
}
