package tasks

import (
	"fmt"
	"reflect"
)

func Task14() {
	a := 1
	b := "1"
	c := true
	d := make(chan int)

	fmt.Println(TypeOf(a))
	fmt.Println(TypeOf(b))
	fmt.Println(TypeOf(c))
	fmt.Println(TypeOf(d))
}

func TypeOf(i interface{}) reflect.Type {
	return reflect.TypeOf(i)
}
