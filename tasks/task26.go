package tasks

import (
	"fmt"
	"strings"
)

func Task26() {
	a := "abcdefg"
	b := "Aa"
	fmt.Println(CheckUnique(a))
	fmt.Println(CheckUnique(b))
}

func CheckUnique(s string) bool {
	s = strings.ToLower(s)
	m := make(map[rune]struct{})
	for _, val := range s {
		if _, ok := m[val]; ok {
			return false
		} else {
			m[val] = struct{}{}
		}
	}
	return true
}
