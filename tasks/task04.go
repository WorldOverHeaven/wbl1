package tasks

import (
	"fmt"
	"time"
)

func Task4() {
	ch := make(chan int)
	i := 0

	RunWorker(3, ch)

	for {
		ch <- i
		i++
		time.Sleep(1 * time.Second)
	}
}

func RunWorker(n int, ch <-chan int) {
	for i := 0; i < n; i++ {
		i := i
		go func(i int) {
			for {
				if v, ok := <-ch; ok {
					fmt.Printf("worker %d receive message: %d\n", i, v)
				} else {
					return
				}
			}
		}(i)
	}
}
