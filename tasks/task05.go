package tasks

import (
	"context"
	"fmt"
	"sync"
	"time"
)

func Task5() {
	sleepTime := 2 * time.Second

	ch := make(chan int)

	go func() {
		i := 0
		for {
			ch <- i
			i++
			time.Sleep(100 * time.Millisecond)
		}
	}()

	wg := sync.WaitGroup{}
	wg.Add(1)

	abort := make(chan struct{})
	go func() {
		for {
			select {
			case x := <-ch:
				fmt.Println(x)
			case <-abort:
				wg.Done()
			}
		}
	}()

	time.Sleep(sleepTime)
	abort <- struct{}{}

	wg.Wait()
}

func T5() {
	ctx, _ := context.WithDeadline(context.Background(), time.Now().Add(time.Second*2))

	ch := make(chan int)

	go func() {
		i := 0
		for {
			select {
			case ch <- i:
				i++
				time.Sleep(100 * time.Millisecond)
			case <-ctx.Done():
				return
			}
		}
	}()

	go func() {
		for {
			select {
			case x := <-ch:
				fmt.Println(x)
			case <-ctx.Done():
				return
			}
		}
	}()

	<-ctx.Done()
}

func TT5() {
	ctx, _ := context.WithTimeout(context.Background(), time.Second*2)

	ch := make(chan int)

	go func() {
		i := 0
		for {
			select {
			case ch <- i:
				i++
				time.Sleep(100 * time.Millisecond)
			case <-ctx.Done():
				return
			}
		}
	}()

	go func() {
		for {
			select {
			case x := <-ch:
				fmt.Println(x)
			case <-ctx.Done():
				return
			}
		}
	}()

	<-ctx.Done()
}
