package tasks

import "fmt"

func Task13() {
	a := 1
	b := 7

	fmt.Println(a, b)
	a, b = b, a
	fmt.Println(a, b)
	a = a ^ b
	b = a ^ b
	a = a ^ b
	fmt.Println(a, b)

	c := make([]int, 2)
	c[0] = 10
	c[1] = 20

	d := make([]int, 2)
	d[0] = 100
	d[1] = 200

	fmt.Println(c, d)
	c, d = d, c
	fmt.Println(c, d)
	// c = c ^ d - так нельзя

}
