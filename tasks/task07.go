package tasks

import (
	"fmt"
	"sync"
)

// RWmutex - это мьютекс чтения записи

type ConcurrentMap struct {
	m  map[int]int
	mu sync.RWMutex
}

func (c *ConcurrentMap) Update(key int, val int) {
	// Блокируем и на чтение и на запись
	c.mu.Lock()
	defer c.mu.Unlock()

	c.m[key] = val
}

func (c *ConcurrentMap) Get(key int) (int, bool) {
	// Блокируем только на запись
	c.mu.RLock()
	defer c.mu.RUnlock()

	val, ok := c.m[key]
	return val, ok
}

func NewConcurrentMap() *ConcurrentMap {
	return &ConcurrentMap{
		m:  make(map[int]int),
		mu: sync.RWMutex{},
	}
}

func Task7() {
	c := NewConcurrentMap()

	wg := sync.WaitGroup{}
	wg.Add(10)

	for i := 0; i < 10; i++ {
		i := i
		go func(i int) {
			c.Update(i, i*i)
			wg.Done()
		}(i)
	}

	wg.Wait()

	fmt.Println(c.m)
}
