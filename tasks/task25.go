package tasks

import (
	"fmt"
	"time"
)

func Task25() {
	fmt.Println("sleep")
	sleep(1000)
	fmt.Println("wake up")

}

func sleep(i int) {
	<-time.After(time.Duration(i) * time.Millisecond)
}
