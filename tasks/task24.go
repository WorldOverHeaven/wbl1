package tasks

import (
	"fmt"
	"math"
)

type Point struct {
	x float64
	y float64
}

func (p1 *Point) Distance(p2 Point) float64 {
	return math.Sqrt(math.Pow(p1.x-p2.x, 2) + math.Pow(p1.y-p2.y, 2))
}

func Distance(p1, p2 Point) float64 {
	return math.Sqrt(math.Pow(p1.x-p2.x, 2) + math.Pow(p1.y-p2.y, 2))
}

func Task24() {
	p1 := Point{
		x: 2.0,
		y: 2.0,
	}

	p2 := Point{
		x: 4.0,
		y: 4.0,
	}

	fmt.Println(Distance(p1, p2))
	fmt.Println(p1.Distance(p2))
}
